DESCRIPTION = "framebuffer based terminal emulator for linux"
LICENSE = "GPLv2"

DEPENDS += "freetype fontconfig ncurses-native ncurses"
RDEPENDS_${PN} = "ncurses-terminfo-base"
RRECOMMENDS_${PN} = "ncurses-terminfo"

inherit autotools pkgconfig

SRC_URI = "\
	 git://salsa.debian.org/debian/fbterm.git;protocol=https \
         file://206df42bb9026a93fecf3ed515e90fe5b21533e7.patch \
         file://vesadev.patch \
"

# TODO : UNTESTED
#SRC_URI_append_libc-musl += "file://c899cbcd131e9ffb17a0e72e870e916a53b3c35e.patch"

LIC_FILES_CHKSUM = "file://COPYING;md5=d8e20eece214df8ef953ed5857862150"

SRCREV = "9c0bbf320e478c29799f9b420fd0aac5951c84a5"

S = "${WORKDIR}/git"



EXTRA_OEMAKE = "\
    'CXXFLAGS=${CXXFLAGS} -I${S}/src/lib' \
    'LDFLAGS=${LDFLAGS} -I${S}/src/lib' \
"


do_configure_prepend() {
  touch ${S}/README
  sed -i -e "s:tic fbterm::g" ${S}/terminfo/Makefile.am
}

