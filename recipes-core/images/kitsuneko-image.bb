inherit core-image features_check

IMAGE_PKGTYPE = "ipk"

IMAGE_NAME = "kitsuneko-${MACHINE}-${DISTRO_VERSION}"

DISTRO_UPDATE_ALTERNATIVES ?= "${PREFERRED_PROVIDER_virtual/update-alternatives}"
DISTRO_PACKAGE_MANAGER ?= "ipkg ipkg-collateral"
ONLINE_PACKAGE_MANAGEMENT = "no"

IMAGE_FEATURES = " \
        debug-tweaks \
	"

IMAGE_INSTALL = "packagegroup-base \
                 packagegroup-core-boot \
                 packagegroup-core-sdk \
                 packagegroup-core-ssh-dropbear \
                 packagegroup-core-x11-base \
                 freerdp \
                 networkmanager \
                 fbterm \
                 ttf-koruri \
                 "

IMAGE_BASENAME = "kitsuneko"
IMAGE_FSTYPES += "tar.gz tar.gz.md5sum"
